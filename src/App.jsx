import {
    IconBell,
    IconBrandApple,
    IconBrandFacebook,
    IconBrandInstagram,
    IconBrandTwitter,
    IconCheck,
    IconCloudOff,
    IconCode,
    IconDatabase,
    IconTrash,
    IconUser,
    IconVideo,
    IconWifiOff,
} from '@tabler/icons';
import Button from './components/Button';
import Card from './components/Card';
import FiturList from './components/FiturList';

import { Navbar } from './components/Navbar';
import Testimonial from './components/Testimonial';
import TestimonialsCard from './components/TestimonialsCard';
import TestimonialsItem from './components/TestimonialsItem';

function App() {
    return (
        <>
            <header className='bg-linear-t  py-8 lg:py-10'>
                <Navbar />
            </header>
            <section className='bg-gradient-to-b from-linear-t to-linear-b pb-20  py-5 lg:py-20'>
                <div className='container mx-auto '>
                    <div className='lg:flex'>
                        <div className='lg:w-1/2'>
                            <h1 className='text-dark text-4xl lg:text-7xl font-medium font-primary tracking-wide leading-tight '>
                                Sebuah cara mudah mengatur jadwal <br /> kuliah kamu
                            </h1>
                            <p className='font-ternary text-xl text-gray-500 leading-relaxed mt-8'>
                                Datang kuliah terlambat karena ketiduran itu tidak masalah; datang kuliah karena lupa jadwal itu sangatlah tidak
                                keren.
                                <br /> Aplikasi ini adalah sahabat kamu sekarang, brodie~
                            </p>
                            <div className='flex mt-10 gap-x-10 '>
                                <Button className=' px-12 lg:px-14 py-3 text-lg font-ternary tracking-wide text-white shadow-xl shadow-s/80 font-medium bg-primary'>
                                    Learn More
                                </Button>
                                <Button className=' px-8 lg:px-14 py-3 text-lg font-ternary tracking-wide  font-medium  bg-secondary border border-s  text-primary'>
                                    Play Demo
                                </Button>
                            </div>
                            <Testimonial />
                        </div>
                        <div className='lg:w-1/2 self-center lg:block hidden pl-14'>
                            <img src='Hero.png' alt='' />
                        </div>
                    </div>
                </div>
            </section>
            <section className='bg-white lg:mt-36 mt-12 mx-auto container pb-12 lg:pb-28'>
                <div className='lg:flex lg:gap-x-20 gap-y-5'>
                    <div className='lg:w-5/12 self-center'>
                        <h1 className='font-primary  text-4xl lg:text-5xl font-semibold text-dark leading-relaxed'>
                            Kenapa Kamu Sangat Harus Pakai Aplikasi <br /> Yang Keren Ini?
                        </h1>
                        <p className='font-ternary text-lg lg:text-2xl font-medium text-gray-400 mt-5 leading-relaxed tracking-wide'>
                            Sejujurnya aplikasi kita ini seringkali bermasalah. Kadang-kadang gak bisa submit data, kadang dibukanya lambat, kadang
                            tiba-tiba logout sendiri, untung gak berdua.
                        </p>
                        <div className='flex mt-10 lg:gap-x-28 gap-x-10 mb-10'>
                            <div className='flex flex-col gap-y-2'>
                                <p className='text-gray-400 font-secondary font-medium text-lg '>TOTAL DON’TLOUD</p>
                                <p className='font-secondary text-dark font-semibold text-2xl'>1,501,234</p>
                            </div>
                            <div className='flex flex-col gap-y-2'>
                                <p className='text-gray-400 font-secondary font-medium text-lg '>TOTAL USERS </p>
                                <p className='font-secondary text-dark font-semibold text-2xl'>1,318,829</p>
                            </div>
                        </div>
                    </div>
                    <div className='lg:w-7/12 '>
                        <div className='flex  flex-col lg:gap-y-10 gap-y-4 '>
                            <div className='flex lg:gap-x-8 gap-x-4'>
                                <div className='lg:p-10 p-4 bg-secondary/30 lg:max-w-sm max-w-xs  rounded-md'>
                                    <div className='flex lg:gap-x-6 gap-x-2'>
                                        <IconDatabase className='bg-secondary/50 p-2 rounded-lg w-14 h-14 text-primary' />
                                        <p className='font-primary text-dark font-semibold text-base lg:text-2xl self-center tracking-wider '>
                                            Aplikasi Gratis
                                        </p>
                                    </div>
                                    <p className='font-secondary text-sm lg:text-lg font-medium tracking-wide mt-5 leading-relaxed'>
                                        Semua fitur di aplikasi ini adalah gratis, tapi data privasi kamu akan kami jual ke agen khusus US.
                                    </p>
                                </div>

                                <div className='lg:p-10 p-4 bg-secondary/30 lg:max-w-sm max-w-xs rounded-md'>
                                    <div className='flex lg:gap-x-6 gap-x-2'>
                                        <IconWifiOff className='bg-secondary/50 p-2 rounded-lg w-14 h-14 text-primary' />
                                        <p className='font-primary text-dark font-semibold text-base lg:text-2xl self-center tracking-wider '>
                                            Kode OTP error
                                        </p>
                                    </div>
                                    <p className='font-secondary text-sm lg:text-lg font-medium tracking-wide mt-5 leading-relaxed'>
                                        Pas login kode OTP lo gak kekirim kadang, terus lo harus nyoba berulang kali sampe bisa.
                                    </p>
                                </div>
                            </div>
                            <div className='flex gap-x-4 lg:gap-x-10 '>
                                <div className='lg:p-10 p-4 bg-secondary/30 lg:max-w-sm max-w-xs  rounded-md'>
                                    <div className='flex lg:gap-x-6 gap-x-2'>
                                        <IconUser className='bg-secondary/50 p-2 rounded-lg w-14 h-14 text-primary' />
                                        <p className='font-primary text-dark font-semibold text-base lg:text-2xl self-center tracking-wider '>
                                            Data Tidak Aman
                                        </p>
                                    </div>
                                    <p className='font-secondary text-sm lg:text-lg font-medium tracking-wide mt-5 leading-relaxed'>
                                        Data tidak disimpan dengan baik dan rentan bocor, jangan heran data lo tiba-tiba ada di deepweb.
                                    </p>
                                </div>
                                <div className='lg:p-10 p-4 bg-secondary/30 lg:max-w-sm max-w-xs  rounded-md'>
                                    <div className='flex lg:gap-x-6 gap-x-2'>
                                        <IconCode className='bg-secondary/50 p-2 rounded-lg w-14 h-14 text-primary' />
                                        <p className='font-primary text-dark font-semibold text-base lg:text-2xl self-center tracking-wider '>
                                            Biasanya Error
                                        </p>
                                    </div>
                                    <p className='font-secondary text-sm lg:text-lg font-medium tracking-wide mt-5 leading-relaxed'>
                                        Ketika submit data biasanya error di bagian ajax-nya, eh nggak deng itu aplikasi pemerintah~
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className='bg-white py-5 lg:py-20  mx-auto container pb-20 lg:pb-28'>
                <div className='lg:flex lg:gap-x-10 gap-y-5'>
                    <div className='lg:w-1/2 '>
                        <div className='lg:max-w-lg rounded-3xl bg-gradient-to-b from-secondary to-primary px-10 py-6 pb-0'>
                            <img src='feature.png' alt='' />
                        </div>
                    </div>
                    <div className='lg:w-1/2 self-center'>
                        <h1 className='font-primary text-3xl py-8 lg:py-10 lg:text-4xl text-dark font-semibold leading-normal tracking-wide'>
                            Memangnya ada fitur apa aja sih di aplikasi yang katanya keren ini? Jadi penasaran nich~
                        </h1>
                        <p className='font-ternary text-lg text-gray-400 leading-relaxed tracking-wide '>
                            Aplikasi sekeren ini akan membuat kamu tidak dapat memberi alasan apapun untuk bolos matkul dosen (HAHA MAMPUS!). Lagian
                            lo kuliah tapi jarang masuk, titip absen doang!
                        </p>
                        <div className='flex flex-col mt-10 gap-y-4'>
                            <div className='flex gap-x-4'>
                                <div className='p-2'>
                                    <IconCloudOff className='text-primary w-12 h-12 stroke-1' />
                                    <h2 className='font-primary text-xl font-medium  text-dark lg:leading-loose leading-normal'>
                                        Tidak Tersimpan Otomatis
                                    </h2>
                                    <p className='font-ternary text-base lg:text-lg tracking-wide text-gray-400 leading-normal lg:leading-relaxed'>
                                        Kalo lo pelupa ya udah dah, jadwal lo bakal gak kesimpen tuh.
                                    </p>
                                </div>
                                <div className='p-2'>
                                    <IconVideo className='text-primary w-12 h-12 stroke-1 ' />
                                    <h2 className='font-primary text-xl font-medium  text-dark leading-normal lg:leading-loose'>
                                        Si Rizal Gak Pernah Ngonten
                                    </h2>
                                    <p className='font-ternary text-base lg:text-lg tracking-wide text-gray-400 leading-normal lg:leading-relaxed'>
                                        Emang sih dia gak pernah ngonten, do’ain ya biar mau ngonten.
                                    </p>
                                </div>
                            </div>
                            <div className='flex gap-x-4'>
                                <div className='p-2'>
                                    <IconTrash className='text-primary w-12 h-12 stroke-1' />
                                    <h2 className='font-primary text-xl font-medium  text-dark leading-normal lg:leading-loose'>Terhapus Otomatis</h2>
                                    <p className='font-ternary text-base lg:text-lg tracking-wide text-gray-400 leading-normal lg:leading-relaxed'>
                                        Data lo bakal kehapus otomatis tiap hari, biar hemat server kita.
                                    </p>
                                </div>
                                <div className='p-2'>
                                    <IconBell className='text-primary w-12 h-12 stroke-1 ' />
                                    <h2 className='font-primary text-xl font-medium  text-dark leading-normal lg:leading-loose'>Nggak Tau Lagi Gw</h2>
                                    <p className='font-ternary  text-base lg:text-lg tracking-wide text-gray-400 leading-normal lg:leading-relaxed'>
                                        Mikir teks buat konten susah bro, ini aja ngasal masih susah!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className='bg-secondary/30 py-10  pb-10 lg:pb-28'>
                <div className='w-full mx-auto container '>
                    <h1 className='font-primary text-2xl lg:text-5xl text-dark leading-relaxed  text-center font-medium tracking-normal  py-10'>
                        Main catur sambil bersepeda, <br /> beda harga beda fitur
                    </h1>
                    <p className='font-ternary font-normal text-lg lg:text-2xl text-gray-400 leading-relaxed text-center '>
                        Aplikasi ini gratis sebenernya, brodie. Tapi kalo mau bayar gapapa juga sih, <br /> untuk gantinya kami kasih fitur yang
                        spesial buat kamu!
                    </p>
                    <div className='lg:flex mt-10 lg:gap-x-8 lg:mb-20 mb-10 gap-y-4'>
                        <Card>
                            <Card.Title>RINDURATU</Card.Title>
                            <Card.Price>Free</Card.Price>
                            <Card.Description>Akses ke semua fitur gratis tapi data lo dijual ke agen khusus US.</Card.Description>
                            <Card.Fitur>
                                <FiturList>Jadwal Tak Terbatas</FiturList>
                                <FiturList>Fitur Notifikasi (Kadang Telat)</FiturList>
                                <FiturList>Mode Gelap</FiturList>
                                <FiturList>Bisa Topup Pulsa</FiturList>
                                <FiturList>Bisa Beli Token Listrik</FiturList>
                                <FiturList>Data Lo Dijual</FiturList>
                            </Card.Fitur>
                            <Card.Footer>
                                <Button className='px-24  lg:px-28 bg-primary font-ternary text-xl py-4  tracking-wide text-white shadow-xl text-center shadow-secondary  '>
                                    Download
                                </Button>
                            </Card.Footer>
                        </Card>

                        <Card>
                            <Card.Title>SULTAN</Card.Title>
                            <Card.Price>Rp 100K</Card.Price>
                            <Card.Description>Sama aja data lo juga tetep dijual, tapi lo dapet fitur yang lain.</Card.Description>
                            <Card.Fitur>
                                <FiturList>Semua Fitur Sebelumnya</FiturList>
                                <FiturList>Diingetin Makan</FiturList>
                                <FiturList>Sleepcall Sampe Bobo</FiturList>
                                <FiturList>Ditemenin Jalan Tiap Minggu</FiturList>
                                <FiturList>Ditemenin Nonton Eksekswan</FiturList>
                                <FiturList>Terhindar Razia Rambut</FiturList>
                                <FiturList>Dapet Izin Bikin Ormas</FiturList>
                            </Card.Fitur>
                            <Card.Footer>
                                <Button className='px-24 lg:px-28 bg-primary font-ternary text-xl py-4  tracking-wide text-white shadow-xl text-center shadow-secondary  '>
                                    Download
                                </Button>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Title>Rafatar</Card.Title>
                            <Card.Price>Rp 10000K</Card.Price>
                            <Card.Description>Ini juga sama aja, data lo dijual juga. Tapi fitur-fiturnya lebih ok.</Card.Description>
                            <Card.Fitur>
                                <FiturList>Semua Fitur Sebelumnya</FiturList>
                                <FiturList>Dibayarin Kuliah 4 Semester</FiturList>
                                <FiturList>Dibayarin Cicilan Motor</FiturList>
                                <FiturList>Dicicilin KPR 12 Tahun</FiturList>
                                <FiturList>Magang di NASA Cab. Depok</FiturList>
                                <FiturList>Dapet Skin Alok</FiturList>
                                <FiturList>Nonton Film Gratis di Ganool</FiturList>
                                <FiturList>Naik Haji Bila Mampu</FiturList>
                            </Card.Fitur>
                            <Card.Footer>
                                <Button className='px-24 lg:px-28 bg-primary font-ternary text-xl py-4  tracking-wide text-white shadow-xl text-center shadow-secondary  '>
                                    Download
                                </Button>
                            </Card.Footer>
                        </Card>
                    </div>
                </div>
                <p className='text-center text-lg font-ternary  font-medium leading-relaxed tracking-wide text-gray-500'>
                    <strong>Harap Diingat: </strong>Harga di atas belum termasuk KKM, dan harga sewaktu-waktu bisa <br /> berubah secara mendadak
                    seperti pengumuman PPKM darurat.
                </p>
            </section>
            <section className='bg-secondary/60 py-10 lg:py-20 pb-28'>
                <div className='container mx-auto'>
                    <h1 className='text-center font-primary text-dark font-semibold text-4xl leading-normal tracking-wider mb-10'>
                        Apa kata umat manusia tentang <br /> aplikasi keren ini?
                    </h1>
                    <p className='text-gray-500 text-center font-ternary font-normal text-lg leading-normal tracking-wide'>
                        Kami tidak memfilter semua review, males soalnya. Jadi semua yang <br /> ditampilkan di sini semuanya asli tanpa reksadana.
                    </p>
                    <TestimonialsItem />
                </div>
            </section>
            <section className='bg-white py-20 pb-10 '>
                <div className='container mx-auto'>
                    <div className='text-center mb-8'>
                        <h1 className='text-dark font-primary font-semibold text-center text-4xl tracking-wide mb-5'>
                            Download Aplikasi Keren Ini Sekarang!
                        </h1>
                        <p className='text-gray-500 font-ternary font-medium text-lg tracking-wide leading-normal'>
                            Please download lah aplikasi ini, udah susah-susah buat, mana pas bikin sampe begadang, <br /> terus juga ditambah
                            weekend. Jadi, please download lah ya.
                        </p>
                    </div>
                    <div className=' flex items-center justify-center gap-x-4 pb-20 lg:pb-36'>
                        <a href='https://apple.com' target='_blank' rel='noreferrer noopener' className=''>
                            <img src='apple.png' alt='' />
                        </a>
                        <a href='https://google.com' target='_blank' rel='noreferrer noopener' className=''>
                            <img src='google.png' alt='' />
                        </a>
                    </div>
                    <hr className='text-slate-600' />
                </div>
            </section>
            <section className='bg-white'>
                <div className='container mx-auto pb-10'>
                    <div className='lg:flex gap-x-10'>
                        <div className='lg:w-5/12  '>
                            <img src='Logo.png' alt='' />
                            <h6 className='text-lg font-primary py-4 tracking-wide'>Zeitplan</h6>
                            <p className='text-base font-ternary text-gray-400 leading-relaxed tracking-wide'>
                                Sebuah aplikasi yang membantu kamu untuk membuat jadwal kuliah, jadwal mabar, jadwal makan, jadwal liga inggris, dan
                                jadwal-jadwal lainnya. Walaupun banyak error, setidaknya kami sudah berusaha semaksimal mungkin.
                            </p>
                            <h6 className='text-base  font-ternary text-gray-400 font-semibold py-4 tracking-wider'>
                                COPYRIGHT (C) 2021. DESIGN BY NAUVAL
                            </h6>
                        </div>
                        <div className='lg:w-2/12 '>
                            <h1 className='text-lg font-primary font-semibold'>Sitemap</h1>
                            <div className='flex flex-col gap-y-4 py-4 font-semibold text-slate-500 font-ternary'>
                                <a href='#'>Beranda</a>
                                <a href='#'>Fitur-fitur</a>
                                <a href='#'>Harga</a>
                                <a href='#'>Testimoni</a>
                                <a href='#'>Download</a>
                            </div>
                        </div>
                        <div className='lg:w-2/12'>
                            <h6 className='text-lg font-primary font-semibold'>Partner</h6>
                            <div className='flex flex-col gap-y-4 py-4 font-semibold text-slate-500  font-ternary'>
                                <a href='#'>Sefan.ru</a>
                                <a href='#'>Ganool</a>
                                <a href='#'>Waptrick</a>
                                <a href='#'>Stafaband</a>
                                <a href='#'>MyWapBlog</a>
                                <a href='#'>Friv</a>
                            </div>
                        </div>
                        <div className='lg:w-3/12'>
                            <h1 className='text-lg font-primary font-semibold'>Tetap Terhubung</h1>
                            <h6 className='font-seconday text-slate-500 py-5'>Lihat kami pansos di sosial media berikut:</h6>
                            <div className='flex flex-row gap-x-10'>
                                <IconBrandInstagram className='p-2 w-12 h-12 bg-secondary/50 stroke-1 text-primary rounded-full ' />
                                <IconBrandTwitter className='p-2 w-12 h-12 bg-secondary/50 stroke-1 text-primary rounded-full ' />
                                <IconBrandFacebook className='p-2 w-12 h-12 bg-secondary/50 stroke-1 text-primary rounded-full ' />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default App;
