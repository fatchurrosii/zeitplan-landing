import React from 'react';

export default function Card({ children }) {
    return (
        <div className='py-5'>
            <div className='px-12 rounded-lg py-10 bg-white shadow-xl shadow-dark/5'>{children}</div>
        </div>
    );
}

function Title({ children }) {
    return <h1 className='uppercase font-secondary text-primary font-semibold text-lg mb-10'>{children}</h1>;
}
function Price({ children }) {
    return <h1 className='font-primary text-dark font-semibold text-4xl lg:text-6xl mb-5'>{children}</h1>;
}
function Description({ children }) {
    return <p className='text-lg font-medium text-gray-400 font-ternary mb-5'>{children}</p>;
}
function Fitur({ children }) {
    return <div className='flex flex-col gap-y-5'>{children}</div>;
}
function Footer({ children }) {
    return <div className='mt-10 '>{children}</div>;
}

Card.Title = Title;
Card.Price = Price;
Card.Description = Description;
Card.Fitur = Fitur;
Card.Footer = Footer;
