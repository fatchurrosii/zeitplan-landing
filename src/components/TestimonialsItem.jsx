import React from 'react';
import TestimonialsCard from './TestimonialsCard';

const items = [
    {
        id: 1,
        review: '“Terima kasih untuk aplikasinya, karena aplikasi ini murid-murid saya jadi rajin kuliah walaupun nggak ada adab. Adab tidak perlu kuliah nomor satu! 👈😎👉”',
        photo: 'unsplash_SE4Xc1WvIkU.png',
        user: 'Sanusi Sulvian',
        role: 'Dosen di Boating School Ny. Puff ',
    },
    {
        id: 2,
        review: '“Gara-gara aplikasi ini aku tidak bisa bolos kuliah, padahal cita-citaku ingin bolos kuliah dan menitipkan absen saja. Huft.”',
        photo: 'unsplash_SE4Xc1WvIkU.png',
        user: 'Wahyu Hayuk',
        role: 'Mahasiswa Abadi',
    },
    {
        id: 3,
        review: '“Hanya di rezim ini ada aplikasi yang membuat anak saya menjadi rajin kuliah walaupun tidak menjadi pandai. Terima kasih Papa Zola!”',
        photo: 'unsplash_SE4Xc1WvIkU.png',
        user: 'Ibu Scarlet Darkening',
        role: 'Ibunya Wahyu Hayuk ',
    },
    {
        id: 4,
        review: '“Aq kmrn koq instla di hp qu gx bs y? ap krn aq pke hp miTho? admin tLonk dnk bntU instlA d hp qu, aq mw pKe apkx”',
        photo: 'unsplash_SE4Xc1WvIkU.png',
        user: 'Indah Cyank Dya Clmax',
        role: 'Works at Looking For True Love, Inc ',
    },
    {
        id: 5,
        review: '“Pengiriman cepat,,, seller ramah,, packing juga rapih, tapi barangnya belom dicoba... saya kasih bintang satu dulu,,,, nanti kalo udah dicoba saya kasih bintang dua,,, nanti kalo udah peterpen jadi bintang di surga... xixixixi... 😀😀😀😀😀”',
        photo: 'unsplash_SE4Xc1WvIkU.png',
        user: 'Jumadi RT. 03',
        role: 'Ketua RT. 06',
    },
    {
        id: '6',
        review: '“aplikasinya keren! mjb, nder.... aku numpang lapak yaaa ... Jual Netflix Spotify disney plus Youtube prem Iqiyi Grammarly Viu Canva Wetv Vidio Mola tv picsart Tezza Unfold Aligh motion Vsco Lightroom get contact apple music amazon prime resso iflix Microsoft 365 HBO GO zoom.”',
        photo: 'unsplash_SE4Xc1WvIkU.png',
        user: 'Cucunya Megalodon',
        role: 'Pecinta Kucing Hungaria ',
    },
];

export default function TestimonialsItem() {
    return (
        <div className='grid lg:grid-cols-3  gap-x-6 gap-y-4 mt-10  '>
            {items.map((item) => (
                <TestimonialsCard key={item.id}>
                    <TestimonialsCard.Review>{item.review}</TestimonialsCard.Review>
                    <div className='flex gap-x-4 mt-2'>
                        <TestimonialsCard.Photo>
                            <img src={item.photo} alt='' />
                        </TestimonialsCard.Photo>
                        <div className='flex flex-col self-center mt-1'>
                            <TestimonialsCard.Name>{item.user}</TestimonialsCard.Name>
                            <TestimonialsCard.Role>{item.role}</TestimonialsCard.Role>
                        </div>
                    </div>
                </TestimonialsCard>
            ))}
        </div>
    );
}
