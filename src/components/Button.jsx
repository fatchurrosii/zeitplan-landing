function Button(props) {
    const { children, text, className = 'bg-primary' } = props;
    return (
        <button
            {...props}
            className={`${className}  [&>svg]:h-5 [&>svg]:w-5 [&>svg]:stroke-2  text-white text-md font-medium px-3 py-2 rounded-md flex items-center gap-x-5`}>
            {text || children}
        </button>
    );
}

export default Button;
