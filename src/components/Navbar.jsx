import { Transition } from '@headlessui/react';
import { IconArrowNarrowRight } from '@tabler/icons';
import { useState, useRef } from 'react';
import Button from './Button';

export const Navbar = () => {
    const [isOpen, setOpen] = useState(false);
    return (
        <div className='container mx-auto  '>
            <div className='py-6  bg-white  rounded-lg shadow-xl shadow-gray-200  '>
                <div className='flex items-center justify-between'>
                    <div className='flex items-center justify-between w-full px-4 lg:px-0'>
                        <div className='flex  items-center gap-x-3  px-4 lg:px-10'>
                            <img src='Logo.png' alt='Zeitplan' />
                            <p className='font-semibold tracking-wide text-lg text-slate-800 uppercase'>Zeitplan</p>
                        </div>

                        <div className='font-normal text-md md:block hidden lg:flex items-center'>
                            <ul className='flex gap-x-10'>
                                <a href='#'>
                                    <li className='text-primary font-medium text-lg'>Home</li>
                                </a>
                                <a href='#'>
                                    <li className='text-gray-400 text-lg'>Fitur - Fitur</li>
                                </a>
                                <a href='#'>
                                    <li className='text-gray-400 text-lg'>Harga</li>
                                </a>
                                <a href='#'>
                                    <li className='text-gray-400 text-lg'>Testimoni</li>
                                </a>
                            </ul>
                            <div className='md:block hidden px-8 w-1/12'>
                                <Button className='shadow-lg shadow-s bg-primary px-6 py-3'>
                                    Download
                                    <IconArrowNarrowRight />
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className='flex  px-4 md:hidden'>
                        <button
                            onClick={() => setOpen(!isOpen)}
                            type='button'
                            className='inline-flex p-2 text-white rounded-md bg-primary focus:outline focus:ring-secondary '
                            aria-controls='mobile-menu'
                            aria-expanded='false'>
                            <span className='sr-only'>Open Main Menu</span>
                            {!isOpen ? (
                                <svg
                                    className='w-6 h-6'
                                    fill='none'
                                    stroke='currentColor'
                                    viewBox='0 0 24 24'
                                    aria-hidden='true'
                                    xmlns='http://www.w3.org/2000/svg'>
                                    <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M4 6h16M4 12h16M4 18h16'></path>
                                </svg>
                            ) : (
                                <svg className='w-6 h-6' fill='none' stroke='currentColor' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
                                    <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M6 18L18 6M6 6l12 12'></path>
                                </svg>
                            )}
                        </button>
                    </div>
                </div>
            </div>
            <Transition
                show={isOpen}
                enter='transition ease-out duration-100 transform'
                enterFrom='opacity-0 scale-95'
                enterTo='opacity-100 scale-100'
                leave='transition ease in duration-74 transform'
                leaveFrom='opacity-100 scale-100'
                leaveTo='opacity-0 scale-95'>
                {(ref) => (
                    <div className='rounded-md shadow-sm md:hidden -mt-5' id='mobile-menu'>
                        <div ref={ref} className='flex flex-col flex-wrap px-4 pt-4 pb-4 space-y-1 bg-white border-gray-400 '>
                            <a
                                href='#'
                                className='px-3 py-2 text-base font-medium rounded-md cursor-pointer text-dark hover:bg-primary hover:text-white'>
                                Home
                            </a>
                            <a
                                href='#'
                                className='px-3 py-2 text-base font-medium rounded-md cursor-pointer text-dark hover:bg-primary hover:text-white'>
                                Fitur - Fitur
                            </a>
                            <a
                                href='#'
                                className='px-3 py-2 text-base font-medium rounded-md cursor-pointer text-dark hover:bg-primary hover:text-white'>
                                Harga
                            </a>
                            <a
                                href='#'
                                className='px-3 py-2 text-base font-medium rounded-md cursor-pointer text-dark hover:bg-primary hover:text-white'>
                                Testimoni
                            </a>
                        </div>
                    </div>
                )}
            </Transition>
        </div>
    );
};
