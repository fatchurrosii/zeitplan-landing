import { IconChevronLeft, IconChevronRight } from '@tabler/icons';
import React from 'react';

const items = [
    {
        id: '1',
        desc: '“Aplikasi sialan, ini gw jadi kagak bisa alesan lagi buat bolos kelas Pak Sanusi yang galak itu! Tapi aku sekarang jadi rajin kuliah, terima kasih garena! ”',
        image: 'michael-dam-mEZ3PoFGs_k-unsplash.jpg',
        author: 'P-7 Star',
        role: 'Mahasiswa Boating School Ny. Puff',
    },
    {
        id: '2',
        desc: '“Terima kasih untuk aplikasinya, karena aplikasi ini murid-murid saya jadi rajin kuliah walaupun nggak ada adab. Adab tidak perlu kuliah nomor satu! 👈😎👉 ”',
        image: 'michael-dam-mEZ3PoFGs_k-unsplash.jpg',
        author: 'Sanusi Sulivan',
        role: 'Dosen di Boating School Ny. Puff ',
    },
    {
        id: '3',
        desc: '“Aq kmrn koq instla di hp qu gx bs y? ap krn aq pke hp miTho? admin tLonk dnk bntU instlA d hp qu, aq mw pKe apkx”',
        image: 'michael-dam-mEZ3PoFGs_k-unsplash.jpg',
        author: 'Indah Cyank Dya Clmax',
        role: 'Works at Looking For True Love, Inc ',
    },
];

export default function Testimonial() {
    return (
        <div className='bg-white p-8 shadow-lg shadow-s/30  mt-10 rounded-lg lg:max-w-xl max-w-md px-6'>
            <p className='font-secondary text-base lg:text-lg font-medium tracking-normal text-dark'>
                “Aplikasi sialan, ini gw jadi kagak bisa alesan lagi buat bolos kelas Pak Sanusi yang galak itu! Tapi aku sekarang jadi rajin kuliah,
                terima kasih garena! ”
            </p>
            <div className='flex gap-x-5 mt-4'>
                <div className='rounded-full w-12 h-12 mt-2 self-center'>
                    <img src='unsplash_SE4Xc1WvIkU.png' alt='' />
                </div>
                <div className='flex flex-col lg:self-center'>
                    <p className='font-ternary text-dark text-base font-semibold'>P-7 Star</p>
                    <p className='font-secondary text-sm text-gray-400 font-medium'>Mahasiswa Boating School Ny. Puff</p>
                </div>
                <div className='flex gap-x-5 self-center lg:ml-20'>
                    <IconChevronLeft className='p-1 text-primary bg-secondary/50 rounded-full w-8 h-8' />

                    <IconChevronRight className='p-1 text-primary bg-secondary/50 rounded-full w-8 h-8' />
                </div>
            </div>
        </div>
    );
}
