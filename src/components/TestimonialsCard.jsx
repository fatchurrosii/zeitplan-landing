import React from 'react';

export default function TestimonialsCard({ children }) {
    return (
        <div className='gap-x-4 gap-y-4'>
            <div className='p-6 max-w-md bg-white rounded-lg'>{children}</div>
        </div>
    );
}
function Review({ children }) {
    return <div className='font-ternary text-dark font-normal leading-normal tracking-normal text-base lg:text-lg'>{children}</div>;
}
function Photo({ children }) {
    return <div className='rounded-full  w-12 h-12 mt-2'>{children}</div>;
}
function Name({ children }) {
    return <h1 className=' text-dark text-lg font-secondary font-semibold '>{children}</h1>;
}
function Role({ children }) {
    return <p className='text-gray-400 text-sm font-ternary tracking-normal '>{children}</p>;
}

TestimonialsCard.Review = Review;
TestimonialsCard.Photo = Photo;
TestimonialsCard.Name = Name;
TestimonialsCard.Role = Role;
