import { IconCheck } from '@tabler/icons';
import React from 'react';

export default function FiturList({ children }) {
    return (
        <div className='flex gap-x-4 '>
            <IconCheck className=' bg-emerald-100 text-emerald-300  rounded-full w-6 h-6' />
            <p className='text-dark font-secondary  text-base lg:text-lg font-medium self-center '>{children}</p>
        </div>
    );
}
